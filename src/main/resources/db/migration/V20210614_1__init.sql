drop table if exists auths;
create table auths
(
    id       serial,
    login    varchar unique,
    password varchar,
    token    varchar,
    user_id  bigint
);

drop table if exists users;
create table users
(
    id   serial,
    name varchar
);

drop table if exists chats;
create table chats
(
    id   serial,
    name varchar
);

drop table if exists participants;
create table participants
(
    id      serial,
    user_id bigint,
    chat_id bigint
);

drop table if exists messages;
create table messages
(
    id      serial,
    user_id bigint,
    chat_id bigint,
    text    varchar
);