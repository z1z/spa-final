package kz.edu.astanait.spafinal.repositories;

import kz.edu.astanait.spafinal.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    void deleteByChatId(Long chatId);
}
