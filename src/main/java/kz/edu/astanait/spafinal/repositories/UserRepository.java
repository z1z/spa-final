package kz.edu.astanait.spafinal.repositories;

import kz.edu.astanait.spafinal.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

}
