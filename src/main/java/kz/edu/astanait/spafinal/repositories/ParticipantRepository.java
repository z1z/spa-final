package kz.edu.astanait.spafinal.repositories;

import kz.edu.astanait.spafinal.models.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {
    boolean existsByChatIdAndUserId(Long chatId, Long userId);
    void deleteAllByChatId(Long chatId);
    void deleteAllByUserId(Long chatId);
}
