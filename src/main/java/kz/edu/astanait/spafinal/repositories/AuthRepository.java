package kz.edu.astanait.spafinal.repositories;

import kz.edu.astanait.spafinal.models.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<Auth,Long> {
    boolean existsByLogin(String login);

    Optional<Auth> findByLogin(String login);

    Optional<Auth> findByToken(String token);

    Optional<Auth> findByUserId(Long userId);
}
