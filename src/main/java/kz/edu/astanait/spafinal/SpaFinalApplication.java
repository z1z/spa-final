package kz.edu.astanait.spafinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpaFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpaFinalApplication.class, args);
	}

}
