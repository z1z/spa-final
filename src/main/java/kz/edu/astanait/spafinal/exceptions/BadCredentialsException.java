package kz.edu.astanait.spafinal.exceptions;

public class BadCredentialsException extends Exception{
    public BadCredentialsException() {
    }

    public BadCredentialsException(String message) {
        super(message);
    }
}
