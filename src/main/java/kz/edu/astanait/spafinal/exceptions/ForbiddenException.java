package kz.edu.astanait.spafinal.exceptions;

public class ForbiddenException extends Exception{
    public ForbiddenException() {
    }

    public ForbiddenException(String message) {
        super(message);
    }
}
