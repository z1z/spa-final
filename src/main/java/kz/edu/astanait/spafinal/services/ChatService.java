package kz.edu.astanait.spafinal.services;

import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.ForbiddenException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.Chat;
import kz.edu.astanait.spafinal.models.Participant;
import kz.edu.astanait.spafinal.models.User;
import kz.edu.astanait.spafinal.repositories.ChatRepository;
import kz.edu.astanait.spafinal.repositories.MessageRepository;
import kz.edu.astanait.spafinal.repositories.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;
    private final ParticipantRepository participantRepository;
    private final MessageRepository messageRepository;

    public Chat add(Chat chat) {
        return chatRepository.save(chat);
    }

    public List<Chat> getAll() {
        return chatRepository.findAll();
    }

    public Chat update(Chat chat, User user) throws BadRequestException, NotFoundException, ForbiddenException {
        if (chat.getId() == null) throw new BadRequestException("Chat id is null.");
        if (!chatRepository.existsById(chat.getId()))
            throw new NotFoundException("Chat not found with id " + chat.getId());
        if (!participantRepository.existsByChatIdAndUserId(chat.getId(), user.getId())) throw new ForbiddenException();
        return chatRepository.save(chat);
    }

    @Transactional
    public void delete(Chat chat, User user) throws BadRequestException, NotFoundException, ForbiddenException {
        if (chat.getId() == null) throw new BadRequestException("Id is null");
        if (!chatRepository.existsById(chat.getId()))
            throw new NotFoundException("Chat not found with id " + chat.getId());
        if (!participantRepository.existsByChatIdAndUserId(chat.getId(), user.getId())) throw new ForbiddenException();
        participantRepository.deleteAllByChatId(chat.getId());
        messageRepository.deleteByChatId(chat.getId());
        chatRepository.delete(chat);
    }

    public void joinChat(Chat chat, User user) {
        if (chatRepository.existsById(chat.getId()))
            if (!participantRepository.existsByChatIdAndUserId(chat.getId(), user.getId())) {
                participantRepository.save(Participant.builder()
                        .chatId(chat.getId())
                        .userId(user.getId())
                        .build()
                );
            }
    }
}
