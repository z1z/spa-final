package kz.edu.astanait.spafinal.services;

import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.User;
import kz.edu.astanait.spafinal.repositories.ParticipantRepository;
import kz.edu.astanait.spafinal.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ParticipantRepository participantRepository;

    public User add(User user) {
        return userRepository.save(user);
    }

    public User getById(Long id) throws NotFoundException {
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User update(User user) throws BadRequestException, NotFoundException {
        if (user.getId() == null) throw new BadRequestException("User id is null.");
        if(!userRepository.existsById(user.getId())) throw new NotFoundException("User not found with id "+ user.getId());
        return userRepository.save(user);
    }

    @Transactional
    public void delete(User user) throws BadRequestException, NotFoundException {
        if (user.getId() == null) throw new BadRequestException("Id is null");
        if(!userRepository.existsById(user.getId())) throw new NotFoundException("User not found with id "+ user.getId());
        participantRepository.deleteAllByUserId(user.getId());
        userRepository.delete(user);
    }
}
