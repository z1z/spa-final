package kz.edu.astanait.spafinal.services;

import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.ForbiddenException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.Message;
import kz.edu.astanait.spafinal.repositories.MessageRepository;
import kz.edu.astanait.spafinal.repositories.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;
    private final ParticipantRepository participantRepository;

    public Message add(Message message) throws ForbiddenException, BadRequestException {
        if (!participantRepository.existsByChatIdAndUserId(message.getChatId(), message.getUserId()))
            throw new ForbiddenException("User is not in this chat");
        if (message.getText() == null || message.getText().isBlank()) throw new BadRequestException("Text is empty");
        return messageRepository.save(message);
    }

    public List<Message> getAll() {
        return messageRepository.findAll();
    }

    public Message update(Message message) throws BadRequestException, NotFoundException, ForbiddenException {
        if (message.getId() == null) throw new BadRequestException("Message id is null.");
        Optional<Message> optionalMessage = messageRepository.findById(message.getId());
        if (optionalMessage.isEmpty()) throw new NotFoundException("Message not found with id " + message.getId());

        Message updatedMessage = optionalMessage.get();
        if (!participantRepository.existsByChatIdAndUserId(message.getChatId(), message.getUserId()))
            throw new ForbiddenException("User is not in this chat");

        if (!message.getUserId().equals(updatedMessage.getUserId())) throw new ForbiddenException();

        updatedMessage.setText(message.getText());

        return messageRepository.save(updatedMessage);
    }

    public void delete(Message message) throws BadRequestException, NotFoundException, ForbiddenException {
        if (message.getId() == null) throw new BadRequestException("Message id is null.");
        Optional<Message> optionalMessage = messageRepository.findById(message.getId());
        if (optionalMessage.isEmpty()) throw new NotFoundException("Message not found with id " + message.getId());

        Message updatedMessage = optionalMessage.get();
        if (!participantRepository.existsByChatIdAndUserId(message.getChatId(), message.getUserId()))
            throw new ForbiddenException("User is not in this chat");

        if (!message.getUserId().equals(updatedMessage.getUserId())) throw new ForbiddenException();

        messageRepository.delete(optionalMessage.get());
    }
}
