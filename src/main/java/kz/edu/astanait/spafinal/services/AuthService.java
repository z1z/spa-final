package kz.edu.astanait.spafinal.services;

import kz.edu.astanait.spafinal.dto.LoginDto;
import kz.edu.astanait.spafinal.dto.RegistrationDto;
import kz.edu.astanait.spafinal.exceptions.BadCredentialsException;
import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.ForbiddenException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.Auth;
import kz.edu.astanait.spafinal.models.User;
import kz.edu.astanait.spafinal.repositories.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UserService userService;

    public Auth add(Auth message) {
        return authRepository.save(message);
    }

    public Auth getByUserId(Long userId) throws NotFoundException {
        return authRepository.findByUserId(userId).orElseThrow(NotFoundException::new);
    }

    public List<Auth> getAll() {
        return authRepository.findAll();
    }

    public Auth update(Auth auth) throws BadRequestException, NotFoundException {
        if (auth.getId() == null) throw new BadRequestException("Auth id is null.");
        Optional<Auth> optionalAuth = authRepository.findById(auth.getId());
        if (optionalAuth.isEmpty())
            throw new NotFoundException("Auth not found with id " + auth.getId());
        Auth updatedAuth = optionalAuth.get();
        updatedAuth.setPassword(auth.getPassword());
        return authRepository.save(updatedAuth);
    }

    public void delete(Auth auth) throws BadRequestException, NotFoundException {
        if (auth.getId() == null) throw new BadRequestException("Id is null");
        if (!authRepository.existsById(auth.getId()))
            throw new NotFoundException("Auth not found with id " + auth.getId());
        authRepository.delete(auth);
    }

    public User register(RegistrationDto registrationDto) throws BadCredentialsException {

        if (authRepository.existsByLogin(registrationDto.getLogin()))
            throw new BadCredentialsException("User exists with login " + registrationDto.getLogin());

        User user = userService.add(User.builder().name(registrationDto.getName()).build());

        Auth auth = Auth.builder()
                .login(registrationDto.getLogin())
                .password(registrationDto.getPassword())
                .userId(user.getId())
                .build();

        authRepository.save(auth);

        return user;
    }

    public String login(LoginDto loginDto) throws NotFoundException, BadCredentialsException, ForbiddenException {
        Optional<Auth> optionalAuth = authRepository.findByLogin(loginDto.getLogin());

        if (optionalAuth.isEmpty()) throw new NotFoundException();

        Auth auth = optionalAuth.get();

        if (!loginDto.getPassword().equals(auth.getPassword()))
            throw new BadCredentialsException("Incorrect password");

        auth.setToken(UUID.randomUUID().toString());

        return authRepository.save(auth).getToken();
    }

    public User checkLogin(String token) throws BadCredentialsException, NotFoundException {
        if (token == null || token.isEmpty()) throw new BadCredentialsException("Token is empty");
        Optional<Auth> optionalAuth = authRepository.findByToken(token);
        if (optionalAuth.isEmpty()) throw new BadCredentialsException("Token is not found");
        return userService.getById(optionalAuth.get().getUserId());
    }
}
