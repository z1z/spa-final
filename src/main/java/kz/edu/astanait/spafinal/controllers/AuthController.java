package kz.edu.astanait.spafinal.controllers;

import kz.edu.astanait.spafinal.dto.LoginDto;
import kz.edu.astanait.spafinal.dto.RegistrationDto;
import kz.edu.astanait.spafinal.exceptions.BadCredentialsException;
import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.ForbiddenException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.Auth;
import kz.edu.astanait.spafinal.models.User;
import kz.edu.astanait.spafinal.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll(@RequestHeader String token) {
        try {
            authService.checkLogin(token);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return ResponseEntity.ok(authService.getAll());
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegistrationDto registrationDto) {
        try {
            User registeredUser = authService.register(registrationDto);
            return ResponseEntity.ok(registeredUser);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto) {
        try {
            String token = authService.login(loginDto);
            return ResponseEntity.ok(token);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (ForbiddenException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/check")
    public ResponseEntity<?> checkLogin(@RequestHeader String token) {
        try {
            User user = authService.checkLogin(token);
            return ResponseEntity.ok(user);
        } catch (BadCredentialsException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Auth auth, @RequestHeader String token) {
        try {
            authService.checkLogin(token);
            return ResponseEntity.ok(authService.update(auth));
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
