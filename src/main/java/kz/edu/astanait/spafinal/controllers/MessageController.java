package kz.edu.astanait.spafinal.controllers;

import kz.edu.astanait.spafinal.exceptions.BadCredentialsException;
import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.ForbiddenException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.Message;
import kz.edu.astanait.spafinal.services.AuthService;
import kz.edu.astanait.spafinal.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll(@RequestHeader String token){
        try {
            authService.checkLogin(token);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return ResponseEntity.ok(messageService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Message message, @RequestHeader String token) {
        try {
            message.setUserId(authService.checkLogin(token).getId());
            return ResponseEntity.ok(messageService.add(message));
        } catch (ForbiddenException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Message message, @RequestHeader String token) {
        try {
            message.setUserId(authService.checkLogin(token).getId());
            return ResponseEntity.ok(messageService.update(message));
        } catch (ForbiddenException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Message message, @RequestHeader String token) {
        try {
            message.setUserId(authService.checkLogin(token).getId());
            messageService.delete(message);
            return ResponseEntity.ok().build();
        } catch (ForbiddenException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
}
