package kz.edu.astanait.spafinal.controllers;

import kz.edu.astanait.spafinal.exceptions.BadCredentialsException;
import kz.edu.astanait.spafinal.exceptions.BadRequestException;
import kz.edu.astanait.spafinal.exceptions.ForbiddenException;
import kz.edu.astanait.spafinal.exceptions.NotFoundException;
import kz.edu.astanait.spafinal.models.Chat;
import kz.edu.astanait.spafinal.models.User;
import kz.edu.astanait.spafinal.services.AuthService;
import kz.edu.astanait.spafinal.services.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")
public class ChatController {
    private final ChatService chatService;
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll(@RequestHeader String token) {
        try {
            authService.checkLogin(token);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return ResponseEntity.ok(chatService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Chat chat, @RequestHeader String token) {
        try {
            authService.checkLogin(token);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }

        return ResponseEntity.ok(chatService.add(chat));
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Chat chat, @RequestHeader String token) {
        try {
            User user = authService.checkLogin(token);
            return ResponseEntity.ok(chatService.update(chat, user));
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ForbiddenException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Chat chat, @RequestHeader String token) {
        try {
            User user = authService.checkLogin(token);
            chatService.delete(chat, user);
            return ResponseEntity.ok().build();
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (BadRequestException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ForbiddenException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/join")
    public ResponseEntity<?> joinChat(@RequestBody Chat chat, @RequestHeader String token) {
        try {
            User user = authService.checkLogin(token);
            chatService.joinChat(chat, user);
            return ResponseEntity.ok().build();
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }
}